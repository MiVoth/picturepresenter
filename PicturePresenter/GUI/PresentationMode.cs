﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PicturePresenter.GUI
{
    public enum PresentationMode
    {
        Picture,
        Svg,
        Html,
        Pdf
    }
}
