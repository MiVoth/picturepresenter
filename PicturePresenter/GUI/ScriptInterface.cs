﻿
namespace PicturePresenter.GUI
{

    [System.Runtime.InteropServices.ComVisibleAttribute(true)]
    public class ScriptInterface
    {
        private readonly PicturePresenter _targetForm;

        public ScriptInterface(PicturePresenter targetForm)
        {
            _targetForm = targetForm;
        }
        public void UpdateValues(long currentPageIndex, bool fitPage)
        {
            _targetForm.CurrentPageIndex = currentPageIndex;
            _targetForm.FitOnPage = fitPage;
        }
    }
}
