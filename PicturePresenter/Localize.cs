﻿using System.Xml.Serialization;

namespace PicturePresenter
{
    [XmlRoot]
    public class Localize
    {
        public Localize()
        {
            PicturePresenter = new PicturePresenterLocalization();
        }
        public string Language { get; set; }

        public PicturePresenterLocalization PicturePresenter { get; set; }
    }

    [System.SerializableAttribute()]
    public class PicturePresenterLocalization
    {
        public string CloseWindow { get; set; }
        public string ShowBlackPicture { get; set; } = "Schwarzes Bild anzeigen";
        public string ShowSelectedPicture { get; set; } = "Ausgewähltes Bild anzeigen";
        public string ClosePresentationWindow { get; set; } = "Bild schließen";
        public string ChooseFolder { get; set; } = "Ordner auswählen";
        public string RefreshFolder { get; set; } = "Aktualisieren";
        public string AvailableScreens { get; set; } = "Verfügbare Bildschirme";
        public string SizeMode { get; set; } = "Anzeigemodus";
        public string SizeModeZoom { get; set; } = "Einpassen";
        public string SizeModeNormal { get; set; } = "Normal";
        public string SizeModeStretch { get; set; } = "Stretchen";
        public string SizeModeCenter { get; set; } = "Zentrieren";
        public string Language { get; set; } = "Sprache";
        public string WebsiteGo { get; set; } = "Anzeigen";
        public string PdfNextPage { get; set; } = "Nächste Seite";
        public string PdfPreviousPage { get; set; } = "Vorherige Seite";
        public string PdfShowPage { get; set; } = "Seite aus Vorschau anzeigen";
        public string PdfFitOnPage { get; set; } = "Ganze Seite anzeigen";
        public string PdfFitWidth { get; set; } = "Seitenbreite";
        public string PdfPage { get; set; } = "Seite";
        public string PdfTotalPages { get; set; } = "Seiten";
        public string RotateRight { get; set; } = "Im Uhrzeigersinn drehen";
        public string RotateLeft { get; set; } = "Gegen Uhrzeigersinn drehen";
    }
}
