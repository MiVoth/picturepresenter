﻿namespace PicturePresenter
{
    partial class PicturePresenter
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PicturePresenter));
            this.btnShowPicture = new System.Windows.Forms.Button();
            this.btnClosePictureWindow = new System.Windows.Forms.Button();
            this.PictureListView = new System.Windows.Forms.ListView();
            this.pictureBoxPreview = new System.Windows.Forms.PictureBox();
            this.btnBlackPicture = new System.Windows.Forms.Button();
            this.btnNextPicture = new System.Windows.Forms.Button();
            this.btnPreviousPicture = new System.Windows.Forms.Button();
            this.btnChooseFolder = new System.Windows.Forms.Button();
            this.rbZoom = new System.Windows.Forms.RadioButton();
            this.rbNormal = new System.Windows.Forms.RadioButton();
            this.rbStretch = new System.Windows.Forms.RadioButton();
            this.rbCenter = new System.Windows.Forms.RadioButton();
            this.btnClose = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnOnlineLibrary = new System.Windows.Forms.Button();
            this.btnBroadcasting = new System.Windows.Forms.Button();
            this.btnJwOrg = new System.Windows.Forms.Button();
            this.txtWebsite = new System.Windows.Forms.TextBox();
            this.btnWebsite = new System.Windows.Forms.Button();
            this.lblWebsite = new System.Windows.Forms.Label();
            this.lblLanguages = new System.Windows.Forms.Label();
            this.drpLanguages = new System.Windows.Forms.ComboBox();
            this.labelSizeModes = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.labelScreens = new System.Windows.Forms.Label();
            this.lbScreen = new System.Windows.Forms.ListBox();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.lblPath = new System.Windows.Forms.Label();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.dateiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.einstellungenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.beendenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.infoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnZoomPlus = new System.Windows.Forms.Button();
            this.panelZoom = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.btnZoomMinus = new System.Windows.Forms.Button();
            this.txtWebZoom = new System.Windows.Forms.TextBox();
            this.btnPdfNext = new System.Windows.Forms.Button();
            this.btnPdfPrevious = new System.Windows.Forms.Button();
            this.panelPdfControls = new System.Windows.Forms.Panel();
            this.btnPdfShowPage = new System.Windows.Forms.Button();
            this.panelPictureControls = new System.Windows.Forms.Panel();
            this.btnRotate270 = new System.Windows.Forms.Button();
            this.btnRotate90 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPreview)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.panelZoom.SuspendLayout();
            this.panelPdfControls.SuspendLayout();
            this.panelPictureControls.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnShowPicture
            // 
            this.btnShowPicture.Location = new System.Drawing.Point(15, 41);
            this.btnShowPicture.Name = "btnShowPicture";
            this.btnShowPicture.Size = new System.Drawing.Size(149, 32);
            this.btnShowPicture.TabIndex = 2;
            this.btnShowPicture.Text = "Ausgewähltes Bild anzeigen";
            this.btnShowPicture.UseVisualStyleBackColor = true;
            this.btnShowPicture.Click += new System.EventHandler(this.BtnShowPicture_Click);
            // 
            // btnClosePictureWindow
            // 
            this.btnClosePictureWindow.Location = new System.Drawing.Point(15, 79);
            this.btnClosePictureWindow.Name = "btnClosePictureWindow";
            this.btnClosePictureWindow.Size = new System.Drawing.Size(149, 32);
            this.btnClosePictureWindow.TabIndex = 3;
            this.btnClosePictureWindow.Text = "Bild schließen";
            this.btnClosePictureWindow.UseVisualStyleBackColor = true;
            this.btnClosePictureWindow.Click += new System.EventHandler(this.BtnClosePictureWindow_Click);
            // 
            // PictureListView
            // 
            this.PictureListView.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.PictureListView.Location = new System.Drawing.Point(6, 177);
            this.PictureListView.Name = "PictureListView";
            this.PictureListView.Size = new System.Drawing.Size(241, 312);
            this.PictureListView.TabIndex = 4;
            this.PictureListView.UseCompatibleStateImageBehavior = false;
            this.PictureListView.SelectedIndexChanged += new System.EventHandler(this.PictureListView_SelectedIndexChanged);
            // 
            // pictureBoxPreview
            // 
            this.pictureBoxPreview.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBoxPreview.Location = new System.Drawing.Point(277, 149);
            this.pictureBoxPreview.Name = "pictureBoxPreview";
            this.pictureBoxPreview.Size = new System.Drawing.Size(640, 360);
            this.pictureBoxPreview.TabIndex = 5;
            this.pictureBoxPreview.TabStop = false;
            // 
            // btnBlackPicture
            // 
            this.btnBlackPicture.Location = new System.Drawing.Point(14, 3);
            this.btnBlackPicture.Name = "btnBlackPicture";
            this.btnBlackPicture.Size = new System.Drawing.Size(150, 32);
            this.btnBlackPicture.TabIndex = 6;
            this.btnBlackPicture.Text = "Schwarzes Bild anzeigen";
            this.btnBlackPicture.UseVisualStyleBackColor = true;
            this.btnBlackPicture.Click += new System.EventHandler(this.BtnBlackPicture_Click);
            // 
            // btnNextPicture
            // 
            this.btnNextPicture.Location = new System.Drawing.Point(920, 149);
            this.btnNextPicture.Name = "btnNextPicture";
            this.btnNextPicture.Size = new System.Drawing.Size(23, 360);
            this.btnNextPicture.TabIndex = 7;
            this.btnNextPicture.Text = ">";
            this.btnNextPicture.UseVisualStyleBackColor = true;
            this.btnNextPicture.Click += new System.EventHandler(this.BtnNextPicture_Click);
            // 
            // btnPreviousPicture
            // 
            this.btnPreviousPicture.Location = new System.Drawing.Point(251, 149);
            this.btnPreviousPicture.Name = "btnPreviousPicture";
            this.btnPreviousPicture.Size = new System.Drawing.Size(23, 360);
            this.btnPreviousPicture.TabIndex = 8;
            this.btnPreviousPicture.Text = "<";
            this.btnPreviousPicture.UseVisualStyleBackColor = true;
            this.btnPreviousPicture.Click += new System.EventHandler(this.BtnPreviousPicture_Click);
            // 
            // btnChooseFolder
            // 
            this.btnChooseFolder.Location = new System.Drawing.Point(5, 495);
            this.btnChooseFolder.Name = "btnChooseFolder";
            this.btnChooseFolder.Size = new System.Drawing.Size(117, 35);
            this.btnChooseFolder.TabIndex = 9;
            this.btnChooseFolder.Text = "Ordner auswählen";
            this.btnChooseFolder.UseVisualStyleBackColor = true;
            this.btnChooseFolder.Click += new System.EventHandler(this.BtnChooseFolder_Click);
            // 
            // rbZoom
            // 
            this.rbZoom.AutoSize = true;
            this.rbZoom.Checked = true;
            this.rbZoom.Location = new System.Drawing.Point(3, 4);
            this.rbZoom.Name = "rbZoom";
            this.rbZoom.Size = new System.Drawing.Size(74, 17);
            this.rbZoom.TabIndex = 10;
            this.rbZoom.TabStop = true;
            this.rbZoom.Text = "Einpassen";
            this.rbZoom.UseVisualStyleBackColor = true;
            this.rbZoom.CheckedChanged += new System.EventHandler(this.RbZoom_CheckedChanged);
            // 
            // rbNormal
            // 
            this.rbNormal.AutoSize = true;
            this.rbNormal.Location = new System.Drawing.Point(3, 27);
            this.rbNormal.Name = "rbNormal";
            this.rbNormal.Size = new System.Drawing.Size(58, 17);
            this.rbNormal.TabIndex = 11;
            this.rbNormal.Text = "Normal";
            this.rbNormal.UseVisualStyleBackColor = true;
            this.rbNormal.CheckedChanged += new System.EventHandler(this.RbNormal_CheckedChanged);
            // 
            // rbStretch
            // 
            this.rbStretch.AutoSize = true;
            this.rbStretch.Location = new System.Drawing.Point(3, 50);
            this.rbStretch.Name = "rbStretch";
            this.rbStretch.Size = new System.Drawing.Size(71, 17);
            this.rbStretch.TabIndex = 12;
            this.rbStretch.Text = "Stretchen";
            this.rbStretch.UseVisualStyleBackColor = true;
            this.rbStretch.CheckedChanged += new System.EventHandler(this.RbStretch_CheckedChanged);
            // 
            // rbCenter
            // 
            this.rbCenter.AutoSize = true;
            this.rbCenter.Location = new System.Drawing.Point(3, 73);
            this.rbCenter.Name = "rbCenter";
            this.rbCenter.Size = new System.Drawing.Size(73, 17);
            this.rbCenter.TabIndex = 14;
            this.rbCenter.Text = "Zentrieren";
            this.rbCenter.UseVisualStyleBackColor = true;
            this.rbCenter.CheckedChanged += new System.EventHandler(this.RbCenter_CheckedChanged);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(848, 3);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(93, 32);
            this.btnClose.TabIndex = 15;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.BtnClose_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btnOnlineLibrary);
            this.panel1.Controls.Add(this.btnBroadcasting);
            this.panel1.Controls.Add(this.btnJwOrg);
            this.panel1.Controls.Add(this.txtWebsite);
            this.panel1.Controls.Add(this.btnWebsite);
            this.panel1.Controls.Add(this.lblWebsite);
            this.panel1.Controls.Add(this.lblLanguages);
            this.panel1.Controls.Add(this.drpLanguages);
            this.panel1.Controls.Add(this.labelSizeModes);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.labelScreens);
            this.panel1.Controls.Add(this.lbScreen);
            this.panel1.Controls.Add(this.btnClose);
            this.panel1.Controls.Add(this.btnBlackPicture);
            this.panel1.Controls.Add(this.btnClosePictureWindow);
            this.panel1.Controls.Add(this.btnShowPicture);
            this.panel1.Location = new System.Drawing.Point(-10, 22);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(961, 122);
            this.panel1.TabIndex = 16;
            // 
            // btnOnlineLibrary
            // 
            this.btnOnlineLibrary.Location = new System.Drawing.Point(439, 60);
            this.btnOnlineLibrary.Name = "btnOnlineLibrary";
            this.btnOnlineLibrary.Size = new System.Drawing.Size(114, 23);
            this.btnOnlineLibrary.TabIndex = 28;
            this.btnOnlineLibrary.Text = "Online Library";
            this.btnOnlineLibrary.UseVisualStyleBackColor = true;
            this.btnOnlineLibrary.Click += new System.EventHandler(this.BtnOnlineLibrary_Click);
            // 
            // btnBroadcasting
            // 
            this.btnBroadcasting.Location = new System.Drawing.Point(559, 60);
            this.btnBroadcasting.Name = "btnBroadcasting";
            this.btnBroadcasting.Size = new System.Drawing.Size(114, 23);
            this.btnBroadcasting.TabIndex = 27;
            this.btnBroadcasting.Text = "JW Broadcasting";
            this.btnBroadcasting.UseVisualStyleBackColor = true;
            this.btnBroadcasting.Click += new System.EventHandler(this.BtnBroadcasting_Click);
            // 
            // btnJwOrg
            // 
            this.btnJwOrg.Location = new System.Drawing.Point(319, 60);
            this.btnJwOrg.Name = "btnJwOrg";
            this.btnJwOrg.Size = new System.Drawing.Size(114, 23);
            this.btnJwOrg.TabIndex = 26;
            this.btnJwOrg.Text = "JW.ORG";
            this.btnJwOrg.UseVisualStyleBackColor = true;
            this.btnJwOrg.Click += new System.EventHandler(this.BtnJwOrg_Click);
            // 
            // txtWebsite
            // 
            this.txtWebsite.Location = new System.Drawing.Point(361, 89);
            this.txtWebsite.Name = "txtWebsite";
            this.txtWebsite.Size = new System.Drawing.Size(203, 20);
            this.txtWebsite.TabIndex = 25;
            this.txtWebsite.Text = "https://www.jw.org/";
            // 
            // btnWebsite
            // 
            this.btnWebsite.Location = new System.Drawing.Point(570, 89);
            this.btnWebsite.Name = "btnWebsite";
            this.btnWebsite.Size = new System.Drawing.Size(103, 23);
            this.btnWebsite.TabIndex = 24;
            this.btnWebsite.Text = "Anzeigen";
            this.btnWebsite.UseVisualStyleBackColor = true;
            this.btnWebsite.Click += new System.EventHandler(this.BtnWebsite_Click);
            // 
            // lblWebsite
            // 
            this.lblWebsite.AutoSize = true;
            this.lblWebsite.Location = new System.Drawing.Point(316, 92);
            this.lblWebsite.Name = "lblWebsite";
            this.lblWebsite.Size = new System.Drawing.Size(46, 13);
            this.lblWebsite.TabIndex = 23;
            this.lblWebsite.Text = "Website";
            // 
            // lblLanguages
            // 
            this.lblLanguages.AutoSize = true;
            this.lblLanguages.Location = new System.Drawing.Point(505, 9);
            this.lblLanguages.Name = "lblLanguages";
            this.lblLanguages.Size = new System.Drawing.Size(47, 13);
            this.lblLanguages.TabIndex = 22;
            this.lblLanguages.Text = "Sprache";
            // 
            // drpLanguages
            // 
            this.drpLanguages.FormattingEnabled = true;
            this.drpLanguages.Location = new System.Drawing.Point(570, 4);
            this.drpLanguages.Name = "drpLanguages";
            this.drpLanguages.Size = new System.Drawing.Size(121, 21);
            this.drpLanguages.TabIndex = 21;
            this.drpLanguages.SelectedIndexChanged += new System.EventHandler(this.DrpLanguages_SelectedIndexChanged);
            // 
            // labelSizeModes
            // 
            this.labelSizeModes.AutoSize = true;
            this.labelSizeModes.Location = new System.Drawing.Point(713, 4);
            this.labelSizeModes.Name = "labelSizeModes";
            this.labelSizeModes.Size = new System.Drawing.Size(76, 13);
            this.labelSizeModes.TabIndex = 20;
            this.labelSizeModes.Text = "Anzeigemodus";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.rbZoom);
            this.panel2.Controls.Add(this.rbStretch);
            this.panel2.Controls.Add(this.rbCenter);
            this.panel2.Controls.Add(this.rbNormal);
            this.panel2.Location = new System.Drawing.Point(713, 21);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(129, 96);
            this.panel2.TabIndex = 19;
            // 
            // labelScreens
            // 
            this.labelScreens.AutoSize = true;
            this.labelScreens.Location = new System.Drawing.Point(190, 25);
            this.labelScreens.Name = "labelScreens";
            this.labelScreens.Size = new System.Drawing.Size(115, 13);
            this.labelScreens.TabIndex = 18;
            this.labelScreens.Text = "Verfügbare Bildschirme";
            // 
            // lbScreen
            // 
            this.lbScreen.FormattingEnabled = true;
            this.lbScreen.Location = new System.Drawing.Point(190, 42);
            this.lbScreen.Name = "lbScreen";
            this.lbScreen.Size = new System.Drawing.Size(120, 69);
            this.lbScreen.TabIndex = 17;
            this.lbScreen.SelectedIndexChanged += new System.EventHandler(this.LbScreen_SelectedIndexChanged);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(130, 495);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(116, 35);
            this.btnRefresh.TabIndex = 16;
            this.btnRefresh.Text = "Aktualisieren";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.BtnRefresh_Click);
            // 
            // lblPath
            // 
            this.lblPath.AutoSize = true;
            this.lblPath.Location = new System.Drawing.Point(12, 161);
            this.lblPath.Name = "lblPath";
            this.lblPath.Size = new System.Drawing.Size(35, 13);
            this.lblPath.TabIndex = 17;
            this.lblPath.Text = "label3";
            // 
            // webBrowser1
            // 
            this.webBrowser1.Location = new System.Drawing.Point(294, 161);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(250, 250);
            this.webBrowser1.TabIndex = 23;
            this.webBrowser1.Visible = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dateiToolStripMenuItem,
            this.infoToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(944, 24);
            this.menuStrip1.TabIndex = 24;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // dateiToolStripMenuItem
            // 
            this.dateiToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.einstellungenToolStripMenuItem,
            this.beendenToolStripMenuItem});
            this.dateiToolStripMenuItem.Name = "dateiToolStripMenuItem";
            this.dateiToolStripMenuItem.Size = new System.Drawing.Size(46, 20);
            this.dateiToolStripMenuItem.Text = "Datei";
            // 
            // einstellungenToolStripMenuItem
            // 
            this.einstellungenToolStripMenuItem.Name = "einstellungenToolStripMenuItem";
            this.einstellungenToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.einstellungenToolStripMenuItem.Text = "Einstellungen";
            // 
            // beendenToolStripMenuItem
            // 
            this.beendenToolStripMenuItem.Name = "beendenToolStripMenuItem";
            this.beendenToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.beendenToolStripMenuItem.Text = "Beenden";
            // 
            // infoToolStripMenuItem
            // 
            this.infoToolStripMenuItem.Name = "infoToolStripMenuItem";
            this.infoToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.infoToolStripMenuItem.Text = "Info";
            // 
            // btnZoomPlus
            // 
            this.btnZoomPlus.Location = new System.Drawing.Point(160, 3);
            this.btnZoomPlus.Name = "btnZoomPlus";
            this.btnZoomPlus.Size = new System.Drawing.Size(32, 23);
            this.btnZoomPlus.TabIndex = 29;
            this.btnZoomPlus.Text = "+";
            this.btnZoomPlus.UseVisualStyleBackColor = true;
            this.btnZoomPlus.Click += new System.EventHandler(this.BtnZoomPlus_Click);
            // 
            // panelZoom
            // 
            this.panelZoom.Controls.Add(this.label1);
            this.panelZoom.Controls.Add(this.btnZoomMinus);
            this.panelZoom.Controls.Add(this.txtWebZoom);
            this.panelZoom.Controls.Add(this.btnZoomPlus);
            this.panelZoom.Location = new System.Drawing.Point(276, 508);
            this.panelZoom.Name = "panelZoom";
            this.panelZoom.Size = new System.Drawing.Size(200, 32);
            this.panelZoom.TabIndex = 30;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 32;
            this.label1.Text = "Website-Zoom";
            // 
            // btnZoomMinus
            // 
            this.btnZoomMinus.Location = new System.Drawing.Point(82, 3);
            this.btnZoomMinus.Name = "btnZoomMinus";
            this.btnZoomMinus.Size = new System.Drawing.Size(33, 23);
            this.btnZoomMinus.TabIndex = 31;
            this.btnZoomMinus.Text = "-";
            this.btnZoomMinus.UseVisualStyleBackColor = true;
            this.btnZoomMinus.Click += new System.EventHandler(this.BtnZoomMinus_Click);
            // 
            // txtWebZoom
            // 
            this.txtWebZoom.Location = new System.Drawing.Point(121, 5);
            this.txtWebZoom.Name = "txtWebZoom";
            this.txtWebZoom.Size = new System.Drawing.Size(33, 20);
            this.txtWebZoom.TabIndex = 30;
            this.txtWebZoom.Text = "100";
            // 
            // btnPdfNext
            // 
            this.btnPdfNext.Location = new System.Drawing.Point(253, 6);
            this.btnPdfNext.Name = "btnPdfNext";
            this.btnPdfNext.Size = new System.Drawing.Size(100, 23);
            this.btnPdfNext.TabIndex = 31;
            this.btnPdfNext.Text = "Nächste Seite";
            this.btnPdfNext.UseVisualStyleBackColor = true;
            // 
            // btnPdfPrevious
            // 
            this.btnPdfPrevious.Location = new System.Drawing.Point(3, 6);
            this.btnPdfPrevious.Name = "btnPdfPrevious";
            this.btnPdfPrevious.Size = new System.Drawing.Size(100, 23);
            this.btnPdfPrevious.TabIndex = 32;
            this.btnPdfPrevious.Text = "Vorherige Seite";
            this.btnPdfPrevious.UseVisualStyleBackColor = true;
            // 
            // panelPdfControls
            // 
            this.panelPdfControls.Controls.Add(this.btnPdfShowPage);
            this.panelPdfControls.Controls.Add(this.btnPdfNext);
            this.panelPdfControls.Controls.Add(this.btnPdfPrevious);
            this.panelPdfControls.Location = new System.Drawing.Point(586, 508);
            this.panelPdfControls.Name = "panelPdfControls";
            this.panelPdfControls.Size = new System.Drawing.Size(357, 32);
            this.panelPdfControls.TabIndex = 33;
            this.panelPdfControls.Visible = false;
            // 
            // btnPdfShowPage
            // 
            this.btnPdfShowPage.Location = new System.Drawing.Point(109, 6);
            this.btnPdfShowPage.Name = "btnPdfShowPage";
            this.btnPdfShowPage.Size = new System.Drawing.Size(139, 23);
            this.btnPdfShowPage.TabIndex = 33;
            this.btnPdfShowPage.Text = "Seite aus Vorschau anzeigen";
            this.btnPdfShowPage.UseVisualStyleBackColor = true;
            // 
            // panelPictureControls
            // 
            this.panelPictureControls.Controls.Add(this.btnRotate270);
            this.panelPictureControls.Controls.Add(this.btnRotate90);
            this.panelPictureControls.Location = new System.Drawing.Point(482, 508);
            this.panelPictureControls.Name = "panelPictureControls";
            this.panelPictureControls.Size = new System.Drawing.Size(230, 32);
            this.panelPictureControls.TabIndex = 34;
            // 
            // btnRotate270
            // 
            this.btnRotate270.Location = new System.Drawing.Point(104, 5);
            this.btnRotate270.Name = "btnRotate270";
            this.btnRotate270.Size = new System.Drawing.Size(123, 23);
            this.btnRotate270.TabIndex = 1;
            this.btnRotate270.Text = "Gegen Uhrzeigersinn";
            this.btnRotate270.UseVisualStyleBackColor = true;
            // 
            // btnRotate90
            // 
            this.btnRotate90.Location = new System.Drawing.Point(3, 5);
            this.btnRotate90.Name = "btnRotate90";
            this.btnRotate90.Size = new System.Drawing.Size(95, 23);
            this.btnRotate90.TabIndex = 0;
            this.btnRotate90.Text = "Im Uhrzeigersinn";
            this.btnRotate90.UseVisualStyleBackColor = true;
            // 
            // PicturePresenter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(944, 542);
            this.Controls.Add(this.panelPictureControls);
            this.Controls.Add(this.panelPdfControls);
            this.Controls.Add(this.panelZoom);
            this.Controls.Add(this.webBrowser1);
            this.Controls.Add(this.lblPath);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnPreviousPicture);
            this.Controls.Add(this.btnNextPicture);
            this.Controls.Add(this.pictureBoxPreview);
            this.Controls.Add(this.btnChooseFolder);
            this.Controls.Add(this.PictureListView);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "PicturePresenter";
            this.Text = "Picture Presenter";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPreview)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panelZoom.ResumeLayout(false);
            this.panelZoom.PerformLayout();
            this.panelPdfControls.ResumeLayout(false);
            this.panelPictureControls.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnShowPicture;
        private System.Windows.Forms.Button btnClosePictureWindow;
        private System.Windows.Forms.ListView PictureListView;
        private System.Windows.Forms.PictureBox pictureBoxPreview;
        private System.Windows.Forms.Button btnBlackPicture;
        private System.Windows.Forms.Button btnNextPicture;
        private System.Windows.Forms.Button btnPreviousPicture;
        private System.Windows.Forms.Button btnChooseFolder;
        private System.Windows.Forms.RadioButton rbZoom;
        private System.Windows.Forms.RadioButton rbNormal;
        private System.Windows.Forms.RadioButton rbStretch;
        private System.Windows.Forms.RadioButton rbCenter;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.ListBox lbScreen;
        private System.Windows.Forms.Label labelScreens;
        private System.Windows.Forms.Label labelSizeModes;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblPath;
        private System.Windows.Forms.Label lblLanguages;
        private System.Windows.Forms.ComboBox drpLanguages;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.TextBox txtWebsite;
        private System.Windows.Forms.Button btnWebsite;
        private System.Windows.Forms.Label lblWebsite;
        private System.Windows.Forms.Button btnOnlineLibrary;
        private System.Windows.Forms.Button btnBroadcasting;
        private System.Windows.Forms.Button btnJwOrg;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem dateiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem einstellungenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem beendenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem infoToolStripMenuItem;
        private System.Windows.Forms.Button btnZoomPlus;
        private System.Windows.Forms.Panel panelZoom;
        private System.Windows.Forms.Button btnZoomMinus;
        private System.Windows.Forms.TextBox txtWebZoom;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnPdfNext;
        private System.Windows.Forms.Button btnPdfPrevious;
        private System.Windows.Forms.Panel panelPdfControls;
        private System.Windows.Forms.Button btnPdfShowPage;
        private System.Windows.Forms.Panel panelPictureControls;
        private System.Windows.Forms.Button btnRotate270;
        private System.Windows.Forms.Button btnRotate90;
    }
}

