﻿using PicturePresenter.GUI;
using PicturePresenter.Utils;
using PicturePresenter.ZoomBrowser;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace PicturePresenter
{
    public partial class PicturePresenter : Form
    {
        private Form _presentationWindow;
        private PictureBox _picBox;
        private string _picturePath = @"C:\";
        private PictureBoxSizeMode _sizeMode = PictureBoxSizeMode.Zoom;
        private int _lastIndex;
        private Screen _selectedScreen;
        private MyBrowser _webBrowser;
        private string _pdfHtml;
        private PicturePresenterLocalization _localization;
        public bool FitOnPage = false;
        public long CurrentPageIndex = 0;

        private PresentationMode PresentationMode;
        private PresentationMode PreviewMode;

        private void InitLabels()
        {
            var langNow = Properties.Settings.Default.Language;
            if (!string.IsNullOrEmpty(langNow))
            {
                langNow = $".{langNow}";
            }
            var lpath = $"{AssemblyUtil.AssemblyDirectory}\\Localize\\localize{langNow}.xml";
            string localizeXml;
            if (System.IO.File.Exists(lpath))
            {
                localizeXml = System.IO.File.ReadAllText(lpath);
            }
            else
            {
                if (langNow == ".DE")
                {
                    localizeXml = Properties.Resources.localize_de;
                }
                else
                {
                    localizeXml = Properties.Resources.localize_en;
                }
            }
            var lcl = (Localize)Program.XmlStringToObject(localizeXml, typeof(Localize));
            PicturePresenterLocalization l = lcl.PicturePresenter;
            btnBlackPicture.Text = l.ShowBlackPicture ?? btnBlackPicture.Text;
            btnShowPicture.Text = l.ShowSelectedPicture ?? btnShowPicture.Text;
            btnClosePictureWindow.Text = l.ClosePresentationWindow ?? btnClosePictureWindow.Text;
            btnChooseFolder.Text = l.ChooseFolder ?? btnChooseFolder.Text;
            btnRefresh.Text = l.RefreshFolder ?? btnRefresh.Text;
            labelScreens.Text = l.AvailableScreens ?? labelScreens.Text;
            labelSizeModes.Text = l.SizeMode ?? labelSizeModes.Text;
            rbCenter.Text = l.SizeModeCenter ?? rbCenter.Text;
            rbNormal.Text = l.SizeModeNormal ?? rbNormal.Text;
            rbStretch.Text = l.SizeModeStretch ?? rbStretch.Text;
            rbZoom.Text = l.SizeModeZoom ?? rbZoom.Text;
            btnClose.Text = l.CloseWindow ?? btnClose.Text;
            lblLanguages.Text = l.Language ?? lblLanguages.Text;
            btnWebsite.Text = l.WebsiteGo ?? btnWebsite.Text;
            btnRotate90.Text = l.RotateRight ?? btnRotate90.Text;
            btnRotate270.Text = l.RotateLeft ?? btnRotate270.Text;

            btnPdfNext.Text = l.PdfNextPage ?? btnPdfNext.Text;
            btnPdfPrevious.Text = l.PdfPreviousPage ?? btnPdfPrevious.Text;
            btnPdfShowPage.Text = l.PdfShowPage ?? btnPdfShowPage.Text;
            lblPath.Text = "";
            _localization = l;
            einstellungenToolStripMenuItem.Enabled = false;


        }

        public PicturePresenter()
        {
            InitializeComponent();
            beendenToolStripMenuItem.Click += (o, a) => BtnClose_Click(null, null);
            try
            {
                InitLabels();
            }
            catch (Exception) { }
            try
            {
                SetupProperties();
                SetupListView();
                SetupPdfHtml();

            }
            catch (Exception) { }

            if (System.IO.Directory.Exists(_picturePath))
            {
                LoadPictureFromPath(_picturePath);
            }

        }

        private void SetupProperties()
        {
            StartPosition = FormStartPosition.Manual;
            Top = 0;
            Top = Properties.Settings.Default.WindowTop;
            Left = Properties.Settings.Default.WindowLeft;
            //TopMost = true;
            // setup languages
            foreach (var lang in Properties.Settings.Default.AvailableLanguages)
            {
                drpLanguages.Items.Add(lang);
            }
            drpLanguages.SelectedItem = Properties.Settings.Default.Language ?? "EN";


            // setup SizeMode
            string sMode = Properties.Settings.Default.SizeMode;
            if (!string.IsNullOrEmpty(sMode))
            {
                switch (sMode)
                {
                    case "Zoom":
                        rbZoom.Checked = true;
                        RbZoom_CheckedChanged(null, null);
                        break;
                    case "Center":
                        rbCenter.Checked = true;
                        RbCenter_CheckedChanged(null, null);
                        break;
                    case "Normal":
                        rbNormal.Checked = true;
                        RbNormal_CheckedChanged(null, null);
                        break;
                    case "Stretch":
                        rbStretch.Checked = true;
                        RbStretch_CheckedChanged(null, null);
                        break;
                    default:
                        rbZoom.Checked = true;
                        RbZoom_CheckedChanged(null, null);
                        break;
                }
            }
            else
            {
                _sizeMode = PictureBoxSizeMode.Zoom;
            }

            //setup Screens
            Screen[] screens = Screen.AllScreens;
            foreach (var screen in screens)
            {
                _selectedScreen = screen;
                lbScreen.Items.Add(screen.DeviceName);
            }
            try
            {
                lbScreen.SelectedItem = Properties.Settings.Default.Display;
            }
            catch (Exception)
            {
                lbScreen.SelectedItem = screens.Last().DeviceName;
                throw;
            }

            _picturePath = Properties.Settings.Default.PicturePath ?? _picturePath;
            FormBorderStyle = FormBorderStyle.FixedSingle;
            BackColor = Color.Snow;

            pictureBoxPreview.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBoxPreview.BackColor = Color.Silver;
            StyleButtons(new List<Button>() { btnBlackPicture, btnClosePictureWindow, btnShowPicture, btnChooseFolder, btnClose, btnRefresh });
            var flaties = new[] { btnPreviousPicture, btnNextPicture, btnWebsite, btnJwOrg, btnOnlineLibrary, btnBroadcasting };
            foreach (var flat in flaties)
            {
                flat.FlatStyle = FlatStyle.Flat;
            }

            Text = $"{Text} ({Application.ProductVersion})";


            btnRotate90.Click += (e, args) => RotateImage(true);
            btnRotate270.Click += (e, args) => RotateImage(false);


            Move += (o, e) =>
            {
                Properties.Settings.Default.WindowTop = Top; Properties.Settings.Default.WindowLeft = Left; Properties.Settings.Default.Save();
            };
            btnPdfNext.Click += (o, e) => ClickPdfNext();
            btnPdfPrevious.Click += (o, e) => ClickPdfPrevious();
            btnPdfShowPage.Click += (o, e) =>
            {
                if (_webBrowser == null)
                {
                    BtnShowPicture_Click(null, null);
                }
                _webBrowser.Document.InvokeScript("showPage", new object[] { CurrentPageIndex, FitOnPage });
            };
        }

        private void ClickPdfPrevious()
        {
            if (_webBrowser == null)
            {
                BtnShowPicture_Click(null, null);
            }
            _webBrowser.Document.InvokeScript("btnPreviousClick");
        }
        private void ClickPdfNext()
        {
            if (_webBrowser == null)
            {
                BtnShowPicture_Click(null, null);
            }
            _webBrowser.Document.InvokeScript("btnNextClick");
        }

        private void ListViewKeyDown(object sender, KeyEventArgs args)
        {
            if (args.KeyCode == Keys.Enter)
            {
                BtnShowPicture_Click(null, null);
            }
        }


        private void SetupListView()
        {
            PictureListView.Clear();
            PictureListView.MultiSelect = false;
            PictureListView.KeyDown += new KeyEventHandler(ListViewKeyDown);
            PictureListView.Scrollable = true;
            PictureListView.View = View.Details;
            PictureListView.Columns.Add(new ColumnHeader() { Text = "", Name = "col1" });
            PictureListView.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
            PictureListView.HeaderStyle = ColumnHeaderStyle.None;
            PictureListView.DoubleClick += new EventHandler(DoubleClickMich);
            PictureListView.HideSelection = false;

            infoToolStripMenuItem.Click += (o, e) => MessageBox.Show($"PicturePresenter v{this.GetType().Assembly.GetName().Version.ToString()}"); ;

            webBrowser1.Navigated += new WebBrowserNavigatedEventHandler((o, arg) =>
            {
                //initPictureWindow();
                if (!(webBrowser1.Url.ToString().EndsWith("pdf") || webBrowser1.Url.ToString().EndsWith("svg")))
                {
                    //showBrowser(_wb, webBrowser1.Url.ToString());
                    //_pw.Focus();
                    //_pw.Show();
                }
            });
        }

        private void DoubleClickMich(object sender, EventArgs e)
        {
            var selectedPath = GetSelectedPath();
            if (File.Exists(selectedPath))
            {
                BtnShowPicture_Click(null, null);
            }
            else if (Directory.Exists(selectedPath))
            {
                if (selectedPath.EndsWith(".."))
                {
                    var ind = selectedPath.LastIndexOf("\\");
                    selectedPath = selectedPath.Substring(0, ind);
                    ind = selectedPath.LastIndexOf("\\");
                    if (ind == -1 || selectedPath.Last() == ':')
                    {
                        _picturePath = "";
                        LoadPictureFromPath("");
                        return;

                    }
                    selectedPath = selectedPath.Substring(0, ind);
                    if (!Directory.Exists(selectedPath))
                    {
                        return;
                    }
                }
                _picturePath = selectedPath;
                LoadPictureFromPath(_picturePath);
                Properties.Settings.Default.PicturePath = _picturePath;
                Properties.Settings.Default.Save();
            }
        }

        private void StyleButtons(IEnumerable<Button> buttons)
        {
            foreach (var button in buttons)
            {
                button.FlatStyle = FlatStyle.Flat;
                button.Height = 32;
                button.BackColor = Color.White;
            }
        }

        private void LoadPictureFromPath(string mypath)
        {
            lblPath.Text = _picturePath;
            SetupListView();
            List<string> s = new List<string>() { "jpg", "png", "jpeg", "bmp", "gif" };
            if (Properties.Settings.Default.PictureFormats != null && Properties.Settings.Default.PictureFormats.Count > 0)
            {
                s.Clear();
                var formats = Properties.Settings.Default.PictureFormats;
                foreach (var item in formats)
                {
                    s.Add(item.ToLower());
                }
            }
            PictureListView.SmallImageList = new ImageList();
            PictureListView.SmallImageList.Images.AddRange(new[]
            {
                Properties.Resources.folder_open_full2,
                Properties.Resources.file_image,
                Properties.Resources.reply
            });

            if (string.IsNullOrEmpty(mypath))
            {
                // show volumes
                var drives = System.IO.Directory.GetLogicalDrives();
                foreach (var drive in drives)
                {
                    ListViewItem lvi = new ListViewItem
                    {
                        ImageIndex = 0,
                        Text = drive
                    };
                    PictureListView.Items.Add(lvi);
                }
            }
            else
            {
                var files = Directory
                            .EnumerateFiles(mypath) //<--- .NET 4.5
                            .Where(f => f.Contains("."))
                            .Select(f => new { Filename = f, Ending = f.Split('.')[f.Split('.').Length - 1] })
                            .Where(file => s.Contains(file.Ending.ToLower()))
                            .ToList(); ;
                PictureListView.Items.Add(new ListViewItem("..") { ImageIndex = 2 });
                var directories = Directory.EnumerateDirectories(mypath);
                foreach (var dir in directories)
                {
                    ListViewItem lvi = new ListViewItem
                    {
                        ImageIndex = 0,
                        Text = Path.GetFileName(dir)
                    };
                    PictureListView.Items.Add(lvi);
                }
                foreach (var file in files)
                {
                    try
                    {
                        ListViewItem lvi = new ListViewItem
                        {
                            ImageIndex = 1,
                            Text = Path.GetFileName(file.Filename)
                        };
                        PictureListView.Items.Add(lvi);
                    }
                    catch (Exception)
                    {
                        continue;
                    }
                }
            }
            try
            {
                CreateFileWatcher(mypath);
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.Message);
            }
        }

        private void KeyPressed(object sender, PreviewKeyDownEventArgs args)
        {
            if (PresentationMode != PresentationMode.Pdf)
            {
                switch (args.KeyCode)
                {
                    case Keys.Left:
                    case Keys.Up:
                        BtnPreviousPicture_Click(null, null);
                        break;
                    case Keys.Right:
                    case Keys.Down:
                        BtnNextPicture_Click(null, null);
                        break;
                    case Keys.Escape:
                    case Keys.Q:
                        BtnClosePictureWindow_Click(null, null);
                        break;
                    case Keys.Home:
                        //_lastIndex = -1;
                        ShowIndex(-1);
                        break;
                    case Keys.End:
                        ShowIndex(PictureListView.Items.Count - 1);
                        break;
                }
            }
            else
            {
                //var contr = (Control)sender;
                switch (args.KeyCode)
                {
                    case Keys.Left:
                    case Keys.Up:
                        //ClickPdfPrevious();
                        break;
                    case Keys.Right:
                    case Keys.Down:
                        //ClickPdfNext();
                        break;
                    case Keys.Escape:
                    case Keys.Q:
                        BtnClosePictureWindow_Click(null, null);
                        break;
                }
                _webBrowser.Focus();
                _webBrowser.Document.Focus();
            }
        }

        public void EnterFullScreenMode(Form targetForm)
        {
            targetForm.WindowState = FormWindowState.Normal;
            targetForm.FormBorderStyle = FormBorderStyle.None;
            targetForm.WindowState = FormWindowState.Maximized;
        }

        public void LeaveFullScreenMode(Form targetForm)
        {
            targetForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            targetForm.WindowState = FormWindowState.Normal;
        }

        private void InitPictureWindow()
        {
            //panelZoom.Hide();
            if (_presentationWindow == null || _presentationWindow.IsDisposed)
            {
                _presentationWindow = new Form()
                {
                    Icon = Icon,
                    Text = "Presentation Window",
                    TopMost = true
                };

                EnterFullScreenMode(_presentationWindow);
                _presentationWindow.WindowState = FormWindowState.Minimized;
                _presentationWindow.BackColor = Color.Black;
                _picBox = new PictureBox();


                _webBrowser = new MyBrowser
                {
                    Top = _selectedScreen.WorkingArea.Y,
                    Left = _selectedScreen.WorkingArea.X,
                    Width = _selectedScreen.WorkingArea.Width,
                    Height = _selectedScreen.WorkingArea.Height,
                    Name = "PresentationBrowser"
                };

                _presentationWindow.Controls.Add(_webBrowser);
                _presentationWindow.Controls.Add(_picBox);
                _webBrowser.PreviewKeyDown += (e, args) => KeyPressed(e, args);

                _presentationWindow.PreviewKeyDown += (e, args) =>
                {
                    if (PresentationMode != PresentationMode.Pdf)
                    {
                        KeyPressed(_presentationWindow, args);
                    }
                };
                _picBox.DoubleClick += new EventHandler(DoubleClickMich);
            }
            _presentationWindow.WindowState = FormWindowState.Maximized;

            SetFormLocation(_presentationWindow, _selectedScreen);
            _picBox.Top = 0;
            _picBox.Width = _presentationWindow.Width;
            _picBox.Height = _presentationWindow.Height;
            _picBox.SizeMode = _sizeMode; // PictureBoxSizeMode.Zoom;
            //pb.BackColor = Color.White;
        }

        void ShowBrowser(WebBrowser swb, string path, Control parent = null, string html = null)
        {

            try
            {
                swb.ScriptErrorsSuppressed = true;
                swb.Show();
                if (html != null)
                {
                    swb.Navigate("about:blank");
                    swb.Document.OpenNew(false);
                    swb.Document.Write(html);
                    swb.Refresh();
                }
                else
                {
                    swb.Url = new Uri(path);
                }
                if (parent == null)
                {
                    parent = swb.Parent;
                    swb.Top = 0;
                    swb.Left = 0;
                }
                else
                {
                    swb.Top = parent.Top;
                    swb.Left = parent.Left;
                }
                swb.Width = parent.Width;
                swb.Height = parent.Height;

            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.Message);
            }
        }




        private void SetupPdfHtml()
        {
            if (webBrowser1.ObjectForScripting == null)
            {
                webBrowser1.ObjectForScripting = new ScriptInterface(this);
            }
            string pdfjs = "";
            string pdfjsWorker = "";
#if DEBUG
            var html = new System.Text.StringBuilder(File.ReadAllText($@"{AssemblyUtil.AssemblyDirectory}\web\test.html"));
            pdfjs = File.ReadAllText($@"{AssemblyUtil.AssemblyDirectory}\web\pdf.js"); //Properties.Resources.pdf;
            pdfjsWorker = File.ReadAllText($@"{AssemblyUtil.AssemblyDirectory}\web\pdf.worker.js");  //Properties.Resources.pdf_worker;
#else
            pdfjs = null; // Properties.Resources.pdf;
            pdfjsWorker = null; // Properties.Resources.pdf_worker;

            Stream data = new MemoryStream(Properties.Resources.web); // The original data
            //Stream unzippedEntryStream; // Unzipped data from a file in the archive
            string tmpHtml = null;
            ZipArchive archive = new ZipArchive(data);
            foreach (ZipArchiveEntry entry in archive.Entries)
            {
                if (entry.Name == "pdf.js")
                {
                    StreamReader reader = new StreamReader(entry.Open());
                    pdfjs = reader.ReadToEnd();
                }
                if (entry.Name == "pdf.worker.js")
                {
                    StreamReader reader = new StreamReader(entry.Open());
                    pdfjsWorker = reader.ReadToEnd();
                }
                if (entry.Name == "test.html")
                {
                    StreamReader reader = new StreamReader(entry.Open());
                    tmpHtml = reader.ReadToEnd();
                }
            }
            var html = new System.Text.StringBuilder(tmpHtml);


#endif

            html.Replace("*pdf.js*", $"<script>{pdfjs} {Environment.NewLine}</script>");
            html.Replace("*pdf.worker.js*", $"<script>{pdfjsWorker}{Environment.NewLine}</script>");
            html.Replace("*next*", $"{_localization.PdfNextPage}");
            html.Replace("*prev*", $"{_localization.PdfPreviousPage}");
            html.Replace("*FitPageYes*", $"{_localization.PdfFitOnPage}");
            html.Replace("*FitPageNo*", $"{_localization.PdfFitWidth}");
            html.Replace("*Page*", $"{_localization.PdfPage}");
            html.Replace("*TotalPages*", $"{_localization.PdfTotalPages}");
            _pdfHtml = html.ToString();
        }


        private void ShowPdfOnBrowser(WebBrowser browser, string pdfPath, Control parentControl = null)
        {
            var pdf = File.ReadAllBytes(pdfPath);
            var conv = Convert.ToBase64String(pdf);
            var html = _pdfHtml.Replace("*data*", conv);
            if (parentControl != null)
            {
                html = html.Replace("opacity: 0;", "opacity: 1;");
            }
#if DEBUG
            //File.WriteAllText(@"D:\out.html", html, System.Text.Encoding.UTF8);
#endif
            ShowBrowser(browser, null, parentControl, html);
        }

        private void BtnShowPicture_Click(object sender, EventArgs e)
        {
            InitPictureWindow();
            var pp = GetSelectedPath();
            panelPdfControls.Hide();

            if (!string.IsNullOrEmpty(pp) && File.Exists(pp))
            {
                if (pp.EndsWith("pdf"))
                {
                    PresentationMode = PresentationMode.Pdf;
                    _presentationWindow.Hide();

                    ShowPdfOnBrowser(_webBrowser, pp);

                    panelPdfControls.Show();
                    _presentationWindow.Focus();
                    _presentationWindow.Show();

                }
                else if (pp.EndsWith("html") || pp.EndsWith("htm"))
                {
                    PresentationMode = PresentationMode.Html;
                    _presentationWindow.Hide();

                    ShowBrowser(_webBrowser, pp);
                    _presentationWindow.Focus();
                    _presentationWindow.Show();
                }
                else
                {
                    if (pp.EndsWith("svg"))
                    {
                        PresentationMode = PresentationMode.Svg;

                        try
                        {
                            throw new Exception();
                            //byte[] data = convertSvg(pp, false);
                            //using (var ms = new MemoryStream(data))
                            //{
                            //    _pb.Image = Image.FromStream(ms);
                            //}
                        }
                        catch (Exception)
                        {
                            _presentationWindow.Hide();
                            ShowBrowser(_webBrowser, pp, null, GetSvgHtml(_webBrowser, File.ReadAllText(pp)));
                            _presentationWindow.Focus();
                            _presentationWindow.Show();
                            return;
                        }
                    }
                    else
                    {
                        PresentationMode = PresentationMode.Picture;

                        _picBox.Image = pictureBoxPreview.Image; // Image.FromFile(pp);
                    }
                    _webBrowser.Hide();
                    _presentationWindow.Show();
                }
                _presentationWindow.Focus();
                _presentationWindow.Show();
            }
        }

        private void RotateImage(bool imUhrzeigersinn)
        {
            Image flipImage = pictureBoxPreview.Image;
            if (imUhrzeigersinn)
            {
                flipImage.RotateFlip(RotateFlipType.Rotate270FlipXY);
            }
            else
            {
                flipImage.RotateFlip(RotateFlipType.Rotate90FlipXY);
            }
            pictureBoxPreview.Image = flipImage;
        }

        string GetSvgHtml(WebBrowser swb, string text)
        {
            //var svgWidth = text.IndexOf("width");
            //if (svgWidth > 0)
            //{
            //    var subs = text.Substring(svgWidth + "width".Length + 2, 100);
            //    var ff = subs.IndexOf("\"");
            //    subs = subs.Substring(0, ff);
            //    if (!subs.Contains("%"))
            //    {
            //        int.TryParse(subs, out int width);
            //        if (width == 0)
            //        {
            //            width = 1000;
            //        }
            //    }
            //}
            var style = ""; // "object > svg {  width: 100%;  height: 100%;}"; // $"body, svg {{height: {swb.Height}px; width: {swb.Width}px;}} svg {{display: block; margin: auto; width: {width}px}}";
            // svg {{display: block; margin: auto; width: {width}px}}
            // 
            //style = "";
            //var html = $@"<!DOCTYPE html><html><head><meta http-equiv=""X-UA-Compatible"" content=""IE=9""/>
            //                <style>{style}</style>
            //        </head>
            //        <body>
            //             <svg>{text}</svg>

            //        </body></html>";
            //return html;
            var myobject = $"<object width=\"{swb.Width}\" height=\"{swb.Height}\">{text}</object>";
            var html = $@"<!DOCTYPE html><html><head><meta http-equiv=""X-UA-Compatible"" content=""IE=9""/>
                            <style>{style}</style>
                    </head>
                    <body>
                         {myobject}
                    </body></html>";
            return html;
        }

        private void BtnClosePictureWindow_Click(object sender, EventArgs e)
        {
            if (_presentationWindow != null)
            {
                _presentationWindow.Close();
                _presentationWindow.Dispose();
                _presentationWindow = null;
                _picBox = null;
            }
            RegainFocus();
        }

        private void SetFormLocation(Form form, Screen screen)
        {
            // first method
            form.StartPosition = FormStartPosition.Manual;
            Rectangle bounds = screen.WorkingArea;
            form.SetBounds(bounds.X, bounds.Y, bounds.Width, bounds.Height);
        }

        private void PictureListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            panelPictureControls.Hide();
            panelPdfControls.Hide();
            if (PictureListView.SelectedItems.Count > 0)
            {
                webBrowser1.Navigate("about:blank");
                webBrowser1.Refresh();

                var p = GetSelectedPath();
                if (!string.IsNullOrEmpty(p) && System.IO.File.Exists(p))
                {
                    if (p.EndsWith("pdf"))
                    {
                        PreviewMode = PresentationMode.Pdf;
                        panelPdfControls.Show();
                        pictureBoxPreview.Hide();
                        ShowPdfOnBrowser(webBrowser1, p, pictureBoxPreview);
                    }
                    else if (p.EndsWith("html") || p.EndsWith("htm"))
                    {
                        PreviewMode = PresentationMode.Html;
                        webBrowser1.Top = pictureBoxPreview.Top;
                        webBrowser1.Left = pictureBoxPreview.Left;
                        webBrowser1.Width = pictureBoxPreview.Width;
                        webBrowser1.Height = pictureBoxPreview.Height;

                        //webBrowser1.Url = new Uri(p.Replace("svg", "html"));
                        if (!p.EndsWith("svg"))
                        {
                            PreviewMode = PresentationMode.Svg;
                            webBrowser1.Url = new Uri(p);
                        }
                        pictureBoxPreview.Hide();
                        webBrowser1.Show();

                        //pictureBoxPreview.Image = new System.Drawing.Imaging.Metafile(p);
                    }
                    else
                    {
                        if (p.EndsWith("svg"))
                        {
                            PreviewMode = PresentationMode.Svg;

                            try
                            {
                                throw new Exception();
                                //byte[] data = ConvertSvg(p);
                                //using (var ms = new MemoryStream(data))
                                //{
                                //    pictureBoxPreview.Image = Image.FromStream(ms);
                                //}

                            }
                            catch (Exception)
                            {
                                //webBrowser1.Top = pictureBoxPreview.Top;
                                //webBrowser1.Left = pictureBoxPreview.Left;
                                //webBrowser1.Width = pictureBoxPreview.Width;
                                //webBrowser1.Height = pictureBoxPreview.Height;
                                //webBrowser1.Url = new Uri(p);
                                //pictureBoxPreview.Hide();
                                //webBrowser1.Show();
                                webBrowser1.Hide();
                                var html = GetSvgHtml(webBrowser1, File.ReadAllText(p));
                                ShowBrowser(webBrowser1, null, pictureBoxPreview, html);
                                pictureBoxPreview.Hide();
                                webBrowser1.Show();
                                return;
                            }
                        }
                        else
                        {
                            PreviewMode = PresentationMode.Picture;
                            panelPictureControls.Show();
                            pictureBoxPreview.Image = Image.FromFile(p);
                        }
                        webBrowser1.Hide();
                        pictureBoxPreview.Show();
                        //pictureBoxPreview.Image = Image.FromFile(p);
                    }
                }
            }
            if (PreviewMode != PresentationMode.Pdf)
            {

            }
        }

        private string GetSelectedPath()
        {
            if (PictureListView.SelectedItems.Count > 0)
            {
                var s = PictureListView.SelectedItems[0];
                if (string.IsNullOrEmpty(_picturePath))
                {
                    return s.Text;
                }
                string p = _picturePath;
                if (!p.EndsWith("\\"))
                {
                    p = $@"{_picturePath}\{s.Text}";
                }
                else
                {
                    p = $@"{_picturePath}{s.Text}";
                }
                return p;  //$@"{_picturePath}\{s.Text}";
            }
            return null;
        }


        private void BtnBlackPicture_Click(object sender, EventArgs e)
        {
            // create black image and show it in pw
            int w = 1; int h = 1;
            InitPictureWindow();
            Image resultImage = new Bitmap(w, h, PixelFormat.Format24bppRgb);
            using (Graphics grp = Graphics.FromImage(resultImage))
            {
                grp.FillRectangle(Brushes.Black, 0, 0, w, h);
                try
                {
                    _webBrowser.Hide();
                }
                catch (Exception) { }
                panelZoom.Hide();
                _picBox.Image = resultImage;
                _presentationWindow.Focus();
                _presentationWindow.Show();
            }
        }

        private void BtnChooseFolder_Click(object sender, EventArgs e)
        {
            var f = new FolderBrowserDialog() { SelectedPath = _picturePath };
            f.ShowDialog();

            var sp = f.SelectedPath;
            if (!string.IsNullOrEmpty(sp))
            {
                _picturePath = sp;
                LoadPictureFromPath(sp);
                try
                {
                    Properties.Settings.Default.PicturePath = _picturePath;
                    Properties.Settings.Default.Save();
                }
                catch (Exception) { }
            }
        }

        private void ShowIndex(int index)
        {
            InitPictureWindow();
            int pcount = PictureListView.Items.Count;
            if (_lastIndex == -1 && index == 1)
            {
                index = 0;
            }
            else if (_lastIndex == pcount && index == pcount - 2)
            {
                index = pcount - 1;
            }

            if (index >= 0 && index < pcount)
            {
                int myindex = index;
                PictureListView.Items[myindex].Selected = true;
                BtnShowPicture_Click(null, null);
            }
            else
            {
                BtnBlackPicture_Click(null, null);
            }
            _lastIndex = index;
        }

        private void BtnNextPicture_Click(object sender, EventArgs e)
        {
            int sindex = 0;
            if (PictureListView.SelectedItems.Count > 0)
            {
                var s = PictureListView.SelectedItems[0];
                sindex = s.Index + 1;
            }
            ShowIndex(sindex);

        }
        private void BtnPreviousPicture_Click(object sender, EventArgs e)
        {
            int sindex = 0;
            if (PictureListView.SelectedItems.Count > 0)
            {
                var s = PictureListView.SelectedItems[0];
                sindex = s.Index - 1;
            }
            ShowIndex(sindex);
        }

        private void RbZoom_CheckedChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.SizeMode = "Zoom";
            SetSizeMode(PictureBoxSizeMode.Zoom);
        }

        private void RbNormal_CheckedChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.SizeMode = "Normal";
            SetSizeMode(PictureBoxSizeMode.Normal);
        }

        private void RbStretch_CheckedChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.SizeMode = "Stretch";
            SetSizeMode(PictureBoxSizeMode.StretchImage);
        }

        private void RbCenter_CheckedChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.SizeMode = "Center";
            SetSizeMode(PictureBoxSizeMode.CenterImage);
        }

        private void SetSizeMode(PictureBoxSizeMode mode)
        {
            _sizeMode = mode;
            if (_picBox != null)
            {
                _picBox.SizeMode = mode;
            }
            pictureBoxPreview.SizeMode = mode;
            Properties.Settings.Default.Save();
        }

        private void BtnClose_Click(object sender, EventArgs e)
        {
            RegainFocus();
            Close();
        }

        private void BtnRefresh_Click(object sender, EventArgs e)
        {
            LoadPictureFromPath(_picturePath);
        }

        private void LbScreen_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbScreen.SelectedItem != null)
            {
                var d = lbScreen.SelectedItem.ToString();
                var scr = Screen.AllScreens.Where(x => x.DeviceName == d).FirstOrDefault();
                if (scr != null)
                {
                    _selectedScreen = scr;
                    Properties.Settings.Default.Display = scr.DeviceName;
                    Properties.Settings.Default.Save();
                    BtnClosePictureWindow_Click(null, null);
                }
            }
        }

        private void DrpLanguages_SelectedIndexChanged(object sender, EventArgs e)
        {
            var lang = drpLanguages.SelectedItem.ToString();
            if (!string.IsNullOrEmpty(lang))
            {
                Properties.Settings.Default.Language = lang;
                Properties.Settings.Default.Save();
                InitLabels();
            }
        }

        private void RegainFocus()
        {
            try
            {
                var libname = Properties.Settings.Default.JWLibrary.ToLower();
                var ps = System.Diagnostics.Process.GetProcesses().Where(n => n.ProcessName.ToLower().Contains(libname)).ToArray();
                if (ps != null && ps.Count() > 0)
                {
                    foreach (var currentProcess in ps)
                    {
                        IntPtr hWnd = currentProcess.MainWindowHandle;
                        if (hWnd != IntPtr.Zero)
                        {
                            SetForegroundWindow(hWnd);
                            ShowWindow(hWnd, 1); // User32.SW_MAXIMIZE);
                        }
                    }
                }
            }
            catch (Exception) { }
        }

        [DllImport("user32.dll")]
        static extern bool SetForegroundWindow(IntPtr hWnd);

        // SetFocus will just focus the keyboard on your application, but not bring your process to front.
        // You don't need it here, SetForegroundWindow does the same.
        // Just for documentation.
        [DllImport("user32.dll")]
        static extern IntPtr SetFocus(HandleRef hWnd);

        [DllImport("user32.dll")]
        internal static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        private void BtnWebsite_Click(object sender, EventArgs e)
        {
            InitPictureWindow();
            panelZoom.Show();
            _presentationWindow.Hide();
            ShowBrowser(_webBrowser, txtWebsite.Text);
            _presentationWindow.Focus();
            _presentationWindow.Show();
            txtWebZoom.Text = "100";
        }

        private void BtnJwOrg_Click(object sender, EventArgs e)
        {
            txtWebsite.Text = "https://www.jw.org";
            BtnWebsite_Click(null, null);
        }

        private void BtnOnlineLibrary_Click(object sender, EventArgs e)
        {
            txtWebsite.Text = "https://wol.jw.org";
            BtnWebsite_Click(null, null);
        }

        private void BtnBroadcasting_Click(object sender, EventArgs e)
        {
            txtWebsite.Text = "https://tv.jw.org";
            //txtWebsite.Text = "https://www.google.de";
            BtnWebsite_Click(null, null);
        }

        private FileSystemWatcher watcher;
        public void CreateFileWatcher(string path)
        {
            if (watcher != null)
            {
                watcher.Dispose();
            }
            // Create a new FileSystemWatcher and set its properties.
            watcher = new FileSystemWatcher
            {
                Path = path,
                /* Watch for changes in LastAccess and LastWrite times, and 
                   the renaming of files or directories. */
                NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite
               | NotifyFilters.FileName | NotifyFilters.DirectoryName
            };
            // Only watch text files.
            //watcher.Filter = "*.txt";

            // Add event handlers.
            watcher.Changed += new FileSystemEventHandler(OnChanged);
            watcher.Created += new FileSystemEventHandler(OnChanged);
            watcher.Deleted += new FileSystemEventHandler(OnChanged);
            watcher.Renamed += new RenamedEventHandler(OnChanged);

            // Begin watching.
            watcher.EnableRaisingEvents = true;
        }

        // Define the event handlers.
        private void OnChanged(object source, FileSystemEventArgs e)
        {
            BtnRefresh_Click(null, null);
            // Specify what is done when a file is changed, created, or deleted.
            Console.WriteLine("File: " + e.FullPath + " " + e.ChangeType);
        }

        protected override void OnClosed(EventArgs e)
        {
            if (watcher != null)
            {
                watcher.Dispose();
            }
            base.OnClosed(e);
        }

        private void BtnZoomPlus_Click(object sender, EventArgs e)
        {
            if (int.TryParse(txtWebZoom.Text, out int i))
            {
                i += 10;
                ZoomWeb(i);
                txtWebZoom.Text = i.ToString();
            }
        }

        private void BtnZoomMinus_Click(object sender, EventArgs e)
        {
            if (int.TryParse(txtWebZoom.Text, out int i))
            {
                i -= 10;
                if (i > 0)
                {
                    ZoomWeb(i);
                    txtWebZoom.Text = i.ToString();
                }
            }
        }

        private void ZoomWeb(int zoom)
        {
            if (_webBrowser != null && !_webBrowser.IsBusy)
            {
                _webBrowser.Zoom(zoom);
            }
        }

        //private void OnRenamed(object source, RenamedEventArgs e)
        //{
        //    // Specify what is done when a file is renamed.
        //    Console.WriteLine("File: {0} renamed to {1}", e.OldFullPath, e.FullPath);
        //}
    }
}
