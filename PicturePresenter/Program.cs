﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

namespace PicturePresenter
{
    static class Program
    {
        /// <summary>
        /// Der Haupteinstiegspunkt für die Anwendung.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new PicturePresenter());
        }

        //[DllImport("user32.dll")]
        //static extern bool SetForegroundWindow(IntPtr hWnd);

        //// SetFocus will just focus the keyboard on your application, but not bring your process to front.
        //// You don't need it here, SetForegroundWindow does the same.
        //// Just for documentation.
        //[DllImport("user32.dll")]
        //static extern IntPtr SetFocus(HandleRef hWnd);


        public static object XmlStringToObject(string xml, Type objtype)
        {

            XmlSerializer ser = default(XmlSerializer);
            ser = new XmlSerializer(objtype);

            StringReader stringReader = default(StringReader);
            stringReader = new StringReader(xml);

            XmlTextReader xmlReader = default(XmlTextReader);
            xmlReader = new XmlTextReader(stringReader);

            object obj = null;
            obj = ser.Deserialize(xmlReader);

            xmlReader.Close();
            stringReader.Close();

            return obj;
        }


        public static string ObjectToXmlString(object Obj)
        {

            string xmlstring = null;

            Type t = Obj.GetType();
            XmlSerializer serializer = new XmlSerializer(t);

            MemoryStream myStream = new MemoryStream();
            serializer.Serialize(myStream, Obj);

            StreamReader r = new StreamReader(myStream);
            myStream.Position = 0;
            xmlstring = r.ReadToEnd();

            return xmlstring;
        }
    }
}
