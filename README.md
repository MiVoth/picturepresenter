
## Picture Presenter

![](img/screenshot.png)

Picture Presenter is a free programm, to display image files on a second screen.

Supported file formats are:

*	img, jpeg, png, gif
*	svg (experimental)
*	html, html
*	pdf (experimental)

---

## Download

You can download it [here.](https://bitbucket.org/MiVoth/picturepresenter/downloads/picture.presenter_1.2.2.2.zip)